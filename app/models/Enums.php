<?php

/*
 *  brainchild > Enums.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

class USER_TYPE {

    // Usertypes identification class

    static $ADMIN = 99;
    static $STUDENT_EXTERNAL = 1;
    static $STUDENT_INTERNAL = 2;
    static $MEGA = 3;
    static $DIG = 4;
    static $DEPARTMENT = 5;
    static $EVENT_CO = 6;
    static $GAMES_CO = 7;
    static $PRIZE_CO = 8;
    static $ID_CARD = 9;
    static $REGISTRATION = 10;
    static $ACCOMODATION = 11;

}


class EVENT_TYPE {

    // Event type identification class

    static $TECHNICAL = 0;
    static $NONTECHNICAL = 1;
    static $WORKSHOP = 2;
    static $GAMES = 3;

}


class EVENT_STATUS {

    // Event status identification class

    static $OPEN = 0;
    static $CLOSED = 1;
    static $STARTED = 2;
    static $COMPLETED = 3;
    static $PRIZE = 4;
    static $OVER = 5;

    public static function getName($id) {

        // Returns relevant text information
        // :param: $id - Id of the event status

        if ($id === self::$OPEN) {
            return "Registration Open";
        } elseif ($id === self::$CLOSED) {
            return "Registration Closed";
        } elseif ($id === self::$STARTED) {
            return "Event Started";
        } elseif ($id === self::$COMPLETED) {
            return "Contest Completed";
        } elseif ($id === self::$PRIZE) {
            return "Winners Selected";
        } elseif ($id === self::$OVER) {
            return "Event Completed";
        } else {
            return "Invalid are you drunk ?";
        }
    }

}


class NOTIFICATION_STATUS {

    // Notification status identification class

    static $UNREAD = 0;
    static $READ = 1;

}


class USER_STATUS {

    // Pay status identification class

    static $CREATED = 0;
    static $OTP_SENT = 1;
    static $VERIFIED = 2;
    static $PAYED = 3;
    static $IDNOTISSUED = 2;
    static $IDISSUED = 3;

}


class USER_STATUS_CHECK {

    // User status check integer

    static $STUDENT = 3;
    static $OTP_VERIFIED = 2;

}


class MINDKRAFT {

    // MK main status identification class

    static $UNDER_CONSTRUCTION = 0;
    static $ITS_A_GO = 1;

}


class COLLEGE {

    // Home college

    static $HOME = "Karunya University";

}


class OTP_STATUS {

    // OTP Status

    static $PENDING = 0;
    static $VERIFIED = 1;

}


class SMS_TEMPLATES {

    // SMS templates

    public static function OTP($name, $otp) {
        return "$name your verification code is : $otp. Visit http://mindkraft.org to Login.";
    }

    public static function EVENT_INFO($event, $venue, $time) {
        return "$event has started at $venue $time. Hurry up and all the best!\nRegards: MK17 Team.";
    }

    public static function EVENT_PROMO($event, $venue ,$msg) {
        return "$event at $venue. $msg";
    }

    public static function EVENT_RESULT($name, $event, $venue, $time, $place) {
        return "Congrats $name you have won $place in $event. Prizes are distrubuted at $venue @ $time.";
    }

    public static function EVENT_ATTENDENCE($name, $event, $venue) {
        return "$name, you have attended $event @ $venue.";
    }

    public static function RESET_PASSWORD($code) {
        return "Your password reset code is : $code.";
    }

}