<?php

/******************************************************************************
 *######                 BRAINCHILD(c)  - Mindkraft 2017                ######*
 ******************************************************************************/

/*
 *  brainchild > routes.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 *
 *  :authors: Abiyouth Manickam,  (add ur names too)
 */


/******************************************************************************
 *#####################        IMPORTS GO HERE        ########################*
 ******************************************************************************/


include_once "models/Enums.php";
include_once "models/Registration.php";
include_once "models/OTP.php";
include_once "models/UserManager.php";
include_once "models/Department.php";
include_once "models/Event.php";

/******************************************************************************
 *#####################       PAGE RENDER ROUTES        ######################*
 ******************************************************************************/


$app->get('/', function($request, $response){
    /*
     * Home page
     */
    $session = new RKA\Session();

    // If user logged in display Username and My Events redirect.
    if($session->get('active', false)){
        $user = $session->get('user');
        $name = $session->get('name');

        return $this->view->render($response, 'website/home.twig', [
            'data' => [
                'user'=>$user,
                'name'=>$name
            ]
        ]);
    }

    // Render page when not logged in.
    return $this->view->render($response, 'website/home.twig');

})->setName('page_home');


$app->get('/login/', function($request, $response){
    /*
     * Login page
     */
    $session = new RKA\Session();

    // If user logged in redirect to home.
    if($session->get('active', false)){
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    // Render page when not logged in.
    return $this->view->render($response, 'website/login.twig');

})->setName('page_login');


$app->get('/logout/', function($request, $response){
    /*
     * Logout page
     */

    // Destroy session and Logout.
    RKA\Session::destroy();

    // Redirect to home.
    return $response->withRedirect($this->router->pathFor('page_home'));

})->setName('page_logout');


$app->get('/myevents/',function($request, $response){
    return 'ASDS';
})->setName('page_myevents');


$app->get('/events/',function($request, $response){
    /*
     * Departments page
     */

    $department = new Department($this->db);
    $depts = $department->getDepartments();
    return $this->view->render($response, 'website/depts.twig', ["depts"=>$depts]);

})->setName('page_depts');


$app->get('/events/{dept}/',function($request, $response, $args){

    $event = new Event($this->db);
    $dept = $args['dept'];
    $data = $event->getEvents($dept);
    return $this->view->render($response, 'website/events.twig', ['name'=>$dept, "events"=>$data] );

})->setName('page_events_dept');


$app->get('/games/',function($request, $response){

})->setName('page_games');


$app->get('/workshops/',function($request, $response){

})->setName('page_workshops');


$app->get('/harmony/',function($request, $response){

})->setName('page_harmony');


/******************************************************************************
 *####################         AJAX API ROUTES          ######################*
 ******************************************************************************/


$app->post('/api/login/', function($request, $response) {
    /*
     * AJAX : Login
     */

    $session = new RKA\Session();
    $login_manger = new UserManager($this->db, $session);

    $body = $response->getBody();

    // Check if already logged in.
    if($session->get('active', false))
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Already logged in.']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $user = $data['username'] or NULL;
    $pass = $data['password'] or NULL;

    if($user == NULL or $pass == NULL)
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty values.']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    // Login happens here. Check UserManager.php.
    $result = $login_manger->loginUser($user, $pass);

    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName('api_login');


$app->post('/api/reg/internal/', function($request, $response) {
    /*
     * AJAX : Internal participant registration
     *
     * Input Data
     *  {
     *      "regno": "UL13CS014",
     *      "name": "Abiyouth",
     *      "pass": "password",
     *      "phone": "9600424320",
     *      "gender": "M",
     *      "dob": "1994-04-17"
     *   }
     *
     * Output Data
     *  {
     *      "status": "success"/"fail",
     *      "message": "Relevant Message"
     *  }
     */

    $session = new \RKA\Session();
    $registration = new Registration($this->db, $session);

    $body = $response->getBody();

    // Check if already logged in.
    if($session->get('active', false))
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Already logged in.']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $regno = $data['regno'] or NULL;
    $name = $data['name'] or NULL;
    //$email = $data['email'];
    $gender = $data['gender'] or NULL;
    $phone = $data['phone'] or NULL;
    $dob = $data['dob'] or NULL;
    $pass = $data['pass'] or NULL;

    if( $regno == NULL or $name == NULL or $gender == NULL or $phone == NULL or $dob == NULL or $pass == NULL )
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    // Register happens here. Check Register.php.
    $result = $registration->internal($regno, $name, $pass, $phone, $gender, $dob);

    $body->write(json_encode($result));

    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_reg_internal');


$app->post('/api/reg/external/', function($request, $response) {
    /*
     * AJAX : External participant registration
     *
     * Input Data
     *  {
     *      "username": "kiru",
     *      "name": "Kiruthika",
     *      "pass": "password",
     *      "email": "kiru@gmail.com"
     *      "phone": "8802200339",
     *      "gender": "F",
     *      "dob": "1995-10-05",
     *      "college": "Stanes College"
     *  }
     *
     * Output Data
     *  {
     *      "status": "success"/"fail",
     *      "message": "Relevant Message"
     *  }
     */

    $session = new \RKA\Session();
    $registration = new Registration($this->db, $session);

    $body = $response->getBody();

    // Check if already logged in.
    if($session->get('active', false))
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Already logged in.']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $regno = $data['username'] or NULL;
    $name = $data['name'] or NULL;
    $email = $data['email'] or NULL;
    $college = $data['college'] or NULL;
    $gender = $data['gender'] or NULL;
    $phone = $data['phone'] or NULL;
    $dob = $data['dob'] or NULL;
    $pass = $data['pass'] or NULL;

    if( $regno == NULL or $name == NULL or $gender == NULL or $phone == NULL or $dob == NULL or $pass == NULL or $college == NULL or $email == NULL )
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }
    // Registration happens here. Check Register.php.
    $result = $registration->external($regno, $name, $pass, $email, $phone, $college, $gender, $dob);

    $body->write(json_encode($result));

    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_reg_external');


$app->post('/api/verify/otp/', function($request, $response) {
    /*
     * AJAX : Verify OTP
     *
     * Input Data
     *  {
     *      "phone": "9600424320",
     *      "pin": "123456"
     *  }
     *
     * Output Data
     *  {
     *       "status": "success"/"fail",
     *       "message": "relevant message"
     *  }
     */

    $session = new \RKA\Session();
    $otp = new OTP($this->db, $session);

    $body = $response->getBody();
    $data = $request->getParsedBody();

    $phone = $data['phone'] or NULL;
    $pin = $data['otp'] or NULL;

    if( $pin == NULL )
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    // OTP verification happens here. Check OTP.php.
    $result = $otp->Verify_OTP($phone, $pin);

    $body->write(json_encode($result));
    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_verify_otp');


$app->post('/api/resend/otp/', function($request, $response) {
    /*
     * AJAX : Resend OTP pin
     *
     * Input Data
     *  {
     *      "phone": "9600424320"
     *  }
     *
     * Output Data
     *  {
     *       "status": "success"/"fail",
     *       "message": "relevant message"
     *  }
     */

    $session = new \RKA\Session();
    $otp = new OTP($this->db, $session);

    $body = $response->getBody();
    $data = $request->getParsedBody();

    $phone = $data['phone'] or NULL;

    if( $phone == NULL )
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    // OTP verification happens here. Check OTP.php.
    $result = $otp->Resend_OTP($phone);

    $body->write(json_encode($result));
    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_resend_otp');


$app->post('/api/reg/event/', function($request, $response, $args) {

})->setName('api_reg_event');


$app->post('/api/reg/game/', function($request, $response, $args) {

})->setName('api_reg_game');


$app->post('/api/submit/feedback/', function($request, $response, $args) {

})->setName('api_submit_feedback');


$app->post('/api/submit/harmony/', function($request, $response, $args) {

})->setName('api_submit_harmony');


$app->post('/api/reset/password/', function($request, $response, $args) {

})->setName('api_reset_password');


/******************************************************************************
 *###################         MOBILE API ROUTES          #####################*
 ******************************************************************************/


/******************************************************************************
 *###################        DEBUG/SPECIAL ROUTES         ####################*
 ******************************************************************************/


/******************************************************************************
 *###################         BRAINCHILD ROUTES          #####################*
 ******************************************************************************/


/******************************************************************************
 *###################       BRAINCHILD AJAX ROUTES       #####################*
 ******************************************************************************/


