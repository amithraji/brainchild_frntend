<?php

/*
 *  brainchild > UserManager.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";

class UserManager
{

    var $db = NULL;
    var $session = NULL;

    public function __construct(&$db, &$session)
    {
        $this->db = &$db;
        $this->session = &$session;
    }

    public function loginUser($username, $password)
    {
        /*
         * Login user
         */

        $user = $this->db->select('users', "*", [
            "AND" => [
                "username" => $username,
                "password" => $password
            ]
        ]);

        if(!count($user))
        {
            return ['status'=>'fail', 'message'=>"Username/Password doesn't exists."];
        }

        if($user[0]['type'] < USER_STATUS_CHECK::$STUDENT)
        {
            if($user[0]['status'] < USER_STATUS_CHECK::$OTP_VERIFIED)
            {
                return ['status'=>'sms_fail','message'=>'OTP Not verified.','phone'=>$user[0]['phone']];
            }
        }

        $this->session->set('active', true);
        $this->session->set('user',$user[0]['username']);
        $this->session->set('type',$user[0]['type']);
        $this->session->set('name',$user[0]['name']);
        $this->session->set('phone',$user[0]['phone']);

        return ['status'=>'success','message'=>'Login Successful'];

    }

}