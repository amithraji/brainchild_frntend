<?php

/*
 *  brainchild > Depts.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

class Department
{
    var $db = NULL;

    public function __construct(&$db)
    {
        $this->db = &$db;
    }

    public function getDepartments()
    {
        $result = $this->db->select('departments',"*");
        return $result;
    }

}